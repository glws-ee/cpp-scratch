#!/usr/bin/env python3
"""Validate the implementation of exercise 3.

Created: 2021-10-30 23:33:27
Author:  Bryant Finney (https://bryant-finney.github.io/about)
"""
import random
import string
import subprocess
import sys
from collections import Counter
from pathlib import Path

assert len(sys.argv) == 2, "the path to the exercise_3 executable must be provided"

executable = Path(sys.argv[1])
assert executable.is_file(), f"executable '{executable}' does not exist"

output = """# Beginning exercise 3

distinct: {distinct}

## Counts

| Character | Count |
| --------- | ----- |
{counts}

Complete!
"""

rand_str = "".join(random.choices(string.digits, k=127))
counter = Counter(rand_str)
distinct = "".join(sorted(counter.keys()))
counts = "\n".join(f"| {k} | {v} |" for k, v in sorted(counter.items()))

with open("expected.md", "w") as f:
    f.write(output.format(distinct=distinct, counts=counts))

with open("out.md", "w") as f:
    subprocess.run([executable, rand_str], check=True, stdout=f)

out = subprocess.run(["diff", "expected.md", "out.md"])
sys.exit(out.returncode)
