#!/bin/sh
# Summary: Validate the implementation of exercise 2.
# Created: 2021-10-30 14:43:48
# Author:  Bryant Finney (https://bryant-finney.github.io/about)

if [ "$#" != "1" ]; then
    echo "a single argument (the executable path) required; received $#"
    exit 1
fi

cat <<EOF >expected.md
# Beginning exercise 2

9: 3^2 + 0^2
10: 3^2 + 1^2
25: 5^2 + 0^2 | 4^2 + 3^2
169: 13^2 + 0^2 | 12^2 + 5^2

Complete!
EOF

$1 9 10 25 169 >out.md

diff expected.md out.md && echo "exercise 2: success ✅"
