#!/bin/sh
# Summary: Define CI tests for exercise 1.
# Created: 2021-10-30 12:18:19
# Author:  Bryant Finney (https://bryant-finney.github.io/about)

if [ "$#" != "1" ]; then
    echo "a single argument (the executable path) required; received $#"
    exit 1
fi

cat <<EOF >expected.md
# Beginning exercise 1

## Reverse linked list: iteration

10 items: 10 9 8 7 6 5 4 3 2 1
reversing...
10 items: 1 2 3 4 5 6 7 8 9 10

## Reverse linked list: recursion

10 items: 10 9 8 7 6 5 4 3 2 1
reversing...
10 items: 1 2 3 4 5 6 7 8 9 10

Complete!
EOF

$1 >out.md

diff expected.md out.md && echo "exercise 1: success ✅"
