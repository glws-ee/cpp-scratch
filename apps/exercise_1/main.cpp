#include <com/com.h>
#include <iostream>

using namespace std;

int main() {
    cout << "# Beginning exercise 1" << endl << endl;

    com::test_count();

    cout << "## Reverse linked list: iteration" << endl << endl;
    com::test_reverse_iter();

    cout << endl;
    cout << "## Reverse linked list: recursion" << endl << endl;

    com::test_reverse_recur();

    cout << endl;
    cout << "Complete!" << endl;
}
