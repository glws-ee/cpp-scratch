#include <com/threading.h>
#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int count_chars(char c, string s) {
    int n;
    for (int i = 0; i < s.size(); i++) {
        n += (int)s[i] == c;
    }
    return n;
};

int main(int argc, char* argv[]) {

    threading::Pool pool;
    if (argc < 2)
        throw invalid_argument("one or more arguments must be provided");

    string given = argv[1];
    string distinct = given;

    cout << "# Beginning exercise 3" << endl << endl;

    sort(begin(distinct), end(distinct));
    auto last = unique(begin(distinct), end(distinct));
    distinct.erase(last, end(distinct));

    cout << "distinct: " << distinct << endl << endl;

    for (int i_char = 0; i_char < distinct.size(); i_char++) {
        threading::Job job{ count_chars, distinct[i_char], given };
        pool.add_job(job);
    }

    cout << "## Counts" << endl << endl;

    pool.finish();

    cout << "| Character | Count |" << endl;
    cout << "| --------- | ----- |" << endl;

    for (int i_char = 0; i_char < distinct.size(); i_char++) {
        cout << "| " << distinct[i_char] << " | "
            << pool.get_result(distinct[i_char]) << " |" << endl;
    }

    cout << endl << "Complete!" << endl;
}
