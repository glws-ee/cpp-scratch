FROM registry.gitlab.com/bryant.finney/cpp-scratch/gcc-cmake AS base

RUN apt update && apt install -y locales gdb

WORKDIR /workspaces/cpp-scratch
COPY . .

WORKDIR /workspaces/cpp-scratch/build
RUN cmake .. && cmake --build .

WORKDIR /workspaces/cpp-scratch

CMD ./build/apps/exercise_1
